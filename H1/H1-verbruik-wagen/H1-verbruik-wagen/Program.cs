﻿using System;

namespace H1_verbruik_wagen
{
    class Program
    {
        static void Main(string[] args)
        {
            double literVoor, literNa, kmStandVoor, kmStandNa, gemiddeld;

            Console.Write("Hoeveel liter was er aanwezig voor de rit? ");
            literVoor = double.Parse(Console.ReadLine());
            Console.Write("Hoeveel liter was er aanwezig na de rit? ");
            literNa = double.Parse(Console.ReadLine());
            Console.Write("Wat was de kmstand voor de rit? ");
            kmStandVoor = double.Parse(Console.ReadLine());
            Console.Write("Wat was de kmstand na de rit? ");
            kmStandNa = double.Parse(Console.ReadLine());

            gemiddeld = (100 * (literVoor - literNa) / (kmStandNa - kmStandVoor));

            Console.WriteLine("Het gemiddelt verbruik: " + gemiddeld);
            Console.WriteLine("Het gemiddelt verbruik afgerond: " + Math.Round(gemiddeld));
            Console.WriteLine(-1 + 4 * 6);
        }
    }
}
