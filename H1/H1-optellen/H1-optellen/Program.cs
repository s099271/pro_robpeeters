﻿using System;

namespace H1_optellen
{
    class Program
    {
        static void Main(string[] args)
        {
            int getal1, getal2;

            Console.WriteLine("Geef het eerste getal");
            getal1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Geef het tweede getal");
            getal2 = int.Parse(Console.ReadLine());

            Console.WriteLine("Som is: " + (getal1 + getal2));
        }
    }
}
