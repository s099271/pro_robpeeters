﻿using System;

namespace H1_variabelen_hoofdletters
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputTekst;

            Console.WriteLine("Wat wilt u herhaald zien worden in hoofdletters?");
            inputTekst = Console.ReadLine();

            Console.WriteLine("U invoer in hoofdletter is: " + inputTekst.ToUpper());
        }
    }
}
