﻿using System;

namespace H2_StringInterpolationRuimte
{
    class Program
    {
        static void Main(string[] args)
        {
            double gewicht;

            Console.Write("Geef gewicht: ");
            gewicht = double.Parse(Console.ReadLine());

            Console.WriteLine($"Op Mercurius heb je een schijnbaar gewicht van: {gewicht * 0.38}kg");
            Console.WriteLine($"Op Venus heb je een schijnbaar gewicht van: {gewicht * 0.91}kg");
            Console.WriteLine($"Op Aarde heb je een schijnbaar gewicht van: {gewicht}kg");
            Console.WriteLine($"Op Mars heb je een schijnbaar gewicht van: {gewicht * 0.38}kg");
            Console.WriteLine($"Op Jupiter heb je een schijnbaar gewicht van: {gewicht * 2.34}kg");
            Console.WriteLine($"Op Saturnus heb je een schijnbaar gewicht van: {gewicht * 1.06}kg");
            Console.WriteLine($"Op Uranus heb je een schijnbaar gewicht van: {gewicht * 0.92}kg");
            Console.WriteLine($"Op Neptunus heb je een schijnbaar gewicht van: {gewicht * 1.19}kg");
            Console.WriteLine($"Op pluto heb je een schijnbaar gewicht van: {gewicht * 0.06} kg");
        }
    }
}
