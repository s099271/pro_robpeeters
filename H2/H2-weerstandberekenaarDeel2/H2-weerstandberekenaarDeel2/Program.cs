﻿using System;

namespace H2_weerstandberekenaarDeel2
{
    class Program
    {
        enum WeerstandKleuren
        {Red, Black, White}
        static void Main(string[] args)
        {
            int ring1, ring2, ring3;

            Console.Write("Geef de waarde tussen 0 en 9 van de eerste ring: ");
            ring1 = sbyte.Parse(Console.ReadLine());
            Console.Write("Geef de waarde tussen 0 en 9 van de tweede ring: ");
            ring2 = sbyte.Parse(Console.ReadLine());
            Console.Write("Geef de waarde tussen -2 en 7 van de derde ring: ");
            ring3 = (int.Parse(Console.ReadLine()));
            string totaal = $"{ring1}{ring2 * (int)Math.Pow(10, ring3)} Ohm";

            if (ring1 == 0 && ring2 == 0 )
            {
                totaal = "0 Ohm";
            }
            Console.WriteLine();



            //            Console.ForegroundColor = ConsoleColor.Yellow;
            //            string myname = $@"
            //╔════════╦════════╦════════╦══════════════╗
            //║ RING 1 ║ RING 2 ║ RING 3 ║ Totaal (Ohm) ║
            //╟────────╫────────╫────────╫──────────────╢ 
            //║{ring1}       ║{ring2}       ║{ring3}       ║{totaal}
            //╚════════╩════════╩════════╩══════════════╝
            //                            ";

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(@"╔════════╦════════╦════════╦══════════════╗");
            Console.WriteLine(@"║ RING 1 ║ RING 2 ║ RING 3 ║ Totaal (Ohm) ║");
            Console.WriteLine(@"╟────────╫────────╫────────╫──────────────╢");
            Console.Write(@"║");
            switch (ring1)
            {
                case 0:
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.Write(@"0");
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"       ║");
                    break;
                case 1:
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.Write(@"1       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                case 2:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write(@"2       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                case 3:
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.Write(@"3       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                case 4:
                    Console.Write(@"4       ");
                    Console.Write(@"║");
                    break;
                case 5:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write(@"5       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                case 6:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write(@"6       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                case 7:
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    Console.Write(@"7       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                case 8:
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.Write(@"8       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                case 9:
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write(@"9       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                default:
                    break;
            }
            switch (ring2)
            {
                case 0:
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.Write(@"0");
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"       ║");
                    break;
                case 1:
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.Write(@"1       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                case 2:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write(@"2       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                case 3:
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.Write(@"3       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                case 4:
                    Console.Write(@"4       ");
                    Console.Write(@"║");
                    break;
                case 5:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write(@"5       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                case 6:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write(@"6       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                case 7:
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    Console.Write(@"7       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                case 8:
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.Write(@"8       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                case 9:
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write(@"9       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                default:
                    break;
            }
            switch (ring3)
            {
                case -2:
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.Write(@"-2      ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                case -1:
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.Write(@"-1      ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                case 0:
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.Write(@"0");
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"       ║");
                    break;
                case 1:
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.Write(@"1       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                case 2:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write(@"2       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                case 3:
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.Write(@"3       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                case 4:
                    Console.Write(@"4       ");
                    Console.Write(@"║");
                    break;
                case 5:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write(@"5       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                case 6:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write(@"6       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                case 7:
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    Console.Write(@"7       ");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(@"║");
                    break;
                default:
                    break;
            }
            Console.WriteLine(totaal);
            Console.WriteLine(@"╚════════╩════════╩════════╩══════════════╝");
        }
    }
}
