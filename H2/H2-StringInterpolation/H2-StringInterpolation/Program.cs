﻿using System;

namespace H2_StringInterpolation
{
    class Program
    {
        static void Main(string[] args)
        {
            const int MAALTAFEL = 511;
            for (int i = 0; ; i++)
            {

                Console.Clear();
                int maal = i + 1;
                int resultaat = maal * MAALTAFEL;
                Console.Write($"{maal} * {MAALTAFEL} is {resultaat}");
                Console.ReadLine();
            }
        }
    }
}
