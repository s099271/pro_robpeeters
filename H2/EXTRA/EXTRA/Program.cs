﻿using System;

namespace EXTRA
{
    class Program
    {
        static void Main(string[] args)
        {
            int nummer = 5;

            Console.OutputEncoding = System.Text.Encoding.UTF8;
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("nl-BE");
            System.Threading.Thread.CurrentThread.CurrentCulture = ci;
            Console.WriteLine($"{nummer:C}");
        }
    }
}
