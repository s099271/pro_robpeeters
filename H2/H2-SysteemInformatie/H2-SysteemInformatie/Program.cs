﻿using System;

namespace H2_SysteemInformatie
{
    class Program
    {
        static void Main(string[] args)
        {
            bool is64bit = Environment.Is64BitOperatingSystem;
            string pcName = Environment.MachineName;
            int procCount = Environment.ProcessorCount;
            string userName = Environment.UserName;
            long memory = Environment.WorkingSet;

            string resultaat = $"Uw computer heeft een 64-bit bestuuringssysteem: {is64bit} \n" +
                               $"De naam van uw pc is: {pcName} \n" +
                               $"Uw pc heeft {procCount} processorkernen \n" +
                               $"{userName} is uw gebruikersnaam \n" +
                               $"Je gebruikt {memory} megabytes aan geheugen";

            Console.WriteLine(resultaat);
        }
    }
}
