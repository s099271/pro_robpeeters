﻿using System;

namespace H2_weerstandberekenaarDeel1
{
    class Program
    {
        static void Main(string[] args)
        {
            int ring1, ring2, ring3;

            Console.Write("Geef de waarde van de eerste ring: ");
            ring1 = int.Parse(Console.ReadLine());
            Console.Write("Geef de waarde van de tweede ring: ");
            ring2 = int.Parse(Console.ReadLine());
            Console.Write("Geef de waarde van de derde ring: ");
            ring3 = (int.Parse(Console.ReadLine()));

            ring3 = (int)Math.Pow(10,ring3);

            Console.WriteLine($"Resultaat is {ring1}{ring2*ring3} Ohm, ofwel {ring1}{ring2}*{ring3}");

        }
    }
}
