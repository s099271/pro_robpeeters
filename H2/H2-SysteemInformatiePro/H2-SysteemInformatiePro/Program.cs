﻿using System;
using System.IO;

namespace H2_SysteemInformatiePro
{
    class Program
    {
        static void Main(string[] args)
        {
            long cDriveInBytes = DriveInfo.GetDrives()[0].AvailableFreeSpace;
            long cDriveTotalSize = DriveInfo.GetDrives()[0].TotalSize;

            Console.WriteLine($"Vrije ruimte op jouw c-schijf: {cDriveInBytes} \n" +
                $"Totale ruimte op van jouw c-schijf: {cDriveTotalSize}\n");

            Console.WriteLine("*********************************");
            Console.Write("Geef met nummer 1 t/m 2 aan over welke harde schijf van jouw pc je informatie wenst: ");
            int driveNummer = (int.Parse(Console.ReadLine()) -1);

            long totalSize = DriveInfo.GetDrives()[driveNummer].TotalSize;
            string naamDrive = DriveInfo.GetDrives()[driveNummer].Name;

            Console.WriteLine($"De vrije ruimte van {naamDrive} is {totalSize/1073741824} Gb");

        }
    }
}
