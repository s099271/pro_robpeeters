﻿using System;

namespace H2_shellStarter
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName, arguments;

            Console.Write("Welk commando wil je uitvoeren? ");
            fileName = Console.ReadLine();
            Console.Write("Wil je arguments toevoegen? ");
            arguments = Console.ReadLine();

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo.FileName = fileName;
            process.StartInfo.Arguments = arguments;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.Start(); //start process

            // Read the output (or the error)
            string output = process.StandardOutput.ReadToEnd();
            Console.WriteLine(output.ToUpper());
            string err = process.StandardError.ReadToEnd();
            Console.WriteLine(err);
            //Continue
            Console.WriteLine("Klaar");
        }
    }
}
