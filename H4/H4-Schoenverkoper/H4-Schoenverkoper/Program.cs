﻿using System;

namespace H4_Schoenverkoper
{
    class Program
    {
        static void Main(string[] args)
        {
            int aantalSchoenen, prijs = 20, aantalKorting;
            Console.Write("Vanaf welk aantal schoenen daalt de prijs? ");
            aantalKorting = int.Parse(Console.ReadLine());
            Console.Write("Hoeveel paar schoenen wens je te kopen? ");
            aantalSchoenen = int.Parse(Console.ReadLine());
            if (aantalSchoenen >= aantalKorting)
            {
                prijs = 10;
            }
            Console.WriteLine($"Totaal prijs is: {prijs*aantalSchoenen} Euro" );
        }
    }
}
