﻿using System;

namespace H4_Schrikkeljaar
{
    class Program
    {
        static void Main(string[] args)
        {
            int jaartal;
            string message = "Is geen schrikkeljaar";
            Console.WriteLine("Geef jaartal: ");
            jaartal = int.Parse(Console.ReadLine());
            if ((jaartal % 4) == 0 )
            {
                if ((jaartal % 100) == 0 && (jaartal % 400) == 0)
                {
                    message = "Is een schrikkeljaar";
                    Console.WriteLine($"Het jaar {jaartal} {message}");
                }
                else
                {
                    Console.WriteLine($"Het jaar {jaartal} {message}");
                }
                
            }
            else
            {
                Console.WriteLine($"Het jaar {jaartal} {message}");
            }
            
        }
    }
}
